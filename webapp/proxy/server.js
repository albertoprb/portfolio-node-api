/**
 * Module dependencies.
 */
var express = require('express');
var passport = require('passport');

// Load configurations. If test env, load example file
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var authorizationMiddleware = require('./config/middlewares/authorization');

// Bootstrap passport config
require('./config/passport')(passport);

var app = express();

// Express settings
require('./config/express')(app, passport);

// Bootstrap routes
require('./config/routes')(app, passport, authorizationMiddleware);

// Start the app
require('./config/app')(app);

// Expose app
exports = module.exports = app;