var config = require('../config/config');

/**
 * Index
 */
exports.index = function(req, res) {
  res.render('app.html');
};

/**
 * This is called to load data into the app
 */
exports.getCredentials = function(req, res) {
  var credentials = req.user.toObject();
  credentials.api_endpoint = config.api_endpoint;
  res.send(credentials);
};