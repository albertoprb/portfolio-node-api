/**
 * Index
 */
exports.index = function(req, res) {
  res.render('login.html');
};

/**
 * Logout
 */
exports.logout = function(req, res) {
  req.logout();
  res.redirect('/');
};