module.exports = function(app, passport, authorizationMiddleware) {
    // Auth
    var auth = require('../controllers/auth');

    app.get('/login', auth.index);

    app.post('/login',
      passport.authenticate('local', {
        failureRedirect: '/login',
        failureFlash: true,
        successRedirect: '/'
      }));

    app.get('/logout', auth.logout);


    // App
    var driverlog = require('../controllers/driverlog');

    app.get('/', authorizationMiddleware.ensureLoggedIn, driverlog.index);
    app.get('/credentials', authorizationMiddleware.ensureLoggedIn, driverlog.getCredentials);

};