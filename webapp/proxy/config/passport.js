var LocalStrategy = require('passport-local').Strategy;
var config = require('./config');
var rest = require('restler');
var User = require('../model/user');

module.exports = function (passport) {
  //Serialize sessions
  passport.serializeUser(function (user, done) {
    console.log('Serialize user ' + user);
    return done(null, user.access_token);
  });

  passport.deserializeUser(function (token, done) {
    console.log('Deserialize user with token: ' + token);
    User.findOne({access_token: token}, function (err, user) {
      console.log('User deserialized: ' + user);
      return done(err, user);
    });
  });

  //Use local strategy
  passport.use(new LocalStrategy(
    function (username, password, done) {
      rest.post(config.api_endpoint + config.api_resource.auth,
        {data: {email: username, password: password}})
        .on('complete', function (data, response) {
          if (response.statusCode === 200) {
            console.log('Authenticated successfully');
            data.last_access = new Date();
            User.update({email: data.email}, data, {upsert: true}, function (err) {
              if (err) {
                return done(err);
              }
              return done(null, data);
            });
          }
          else {
            console.log('Authentication problem: ' + response.statusCode);
            return done(null, false);
          }
        });
    })
  );
};