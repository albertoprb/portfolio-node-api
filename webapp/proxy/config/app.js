var config = require('./config');
var mongoose = require('mongoose');

module.exports = function(app) {
  mongoose.connect(config.db_url + config.db_name);
  var db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open', function(){
    app.listen(config.port);
    console.log('Driverlog webapp started on port ' + config.port);
  });
};