var path = require('path'),
rootPath = path.normalize(__dirname + '/../../..');

module.exports = {
  root: rootPath,
  api_resource: {
    auth:'/user/token',
    user:'/user',
    trips:'/trip'
  },
  db_name:"driverlogwebapp",
	port: process.env.PORT || 8080,
  cookie_secret: 'FLEET=*=STER'
};
