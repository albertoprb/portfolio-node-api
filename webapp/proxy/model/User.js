var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
	'email':{type: String, unique:true, required: true},
	'access_token':{type: String},
	'last_access':{type: Date},
  'session':{type:String}
});

var UserModel = mongoose.model('User', UserSchema);

module.exports = UserModel;