'use strict';

module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  // configurable paths
  var config = {
    app: 'app',
    dist: 'dist'
  };

  try {
    config.app = require('./bower.json').appPath || config.app;
  } catch (e) {}

  grunt.initConfig({
    yeoman: config,

    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true
      }
    }
  });

  grunt.registerTask('test', [

  ]);

  grunt.registerTask('build', [

  ]);

  grunt.registerTask('default', [
    'test',
    'build'
  ]);
};
