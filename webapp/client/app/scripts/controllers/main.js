'use strict';

angular.module('driverlog')
  .controller('MainCtrl', function ($scope, CredentialService) {
    var exports = {};
    CredentialService.get(function (data){
      exports.credentials = data;
    });
    $scope.MainCtrl = exports;
  });