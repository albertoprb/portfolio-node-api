'use strict';

angular.module('driverlog')
  .controller('AccountCtrl', function ($scope, CredentialService) {
    var exports = {};
    CredentialService.get(function (data){
      exports.credentials = data;
    });
    $scope.AccountCtrl = exports;
  });