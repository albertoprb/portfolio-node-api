'use strict';

angular.module('driverlog').factory('CredentialService', ['$http', function($http){
  var service = {};
  service.get = function(callback) {
    $http({method: 'GET', url: '/credentials', cache:true}). success(function(data) {
      callback(data);
    });
  };
  return service;
}]);