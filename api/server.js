var express = require('express');
var http = require('http');
var path = require('path');
var mongoose = require('mongoose');
var passport = require('./services/AuthService');

var app = express();

app.configure(function () {
  app.set('port', process.env.PORT || 3000);

  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());

  app.use(passport.initialize());
  app.use(passport.session());
});

app.configure('development', function () {
  app.use(express.errorHandler());
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
  mongoose.connect('mongodb://localhost/driverlogapi');
});

app.configure('production', function () {
  app.use(express.compress());
  mongoose.connect('mongodb://driverlog:driverlog@paulo.mongohq.com:10017/driverlogapi');
});

http.createServer(app).listen(app.get('port'), function () {

  var environment = process.env.NODE_ENV || 'development';

  openDatabase(function callback() {

    require('./api/TripRouter')(app);
    require('./api/UserRouter')(app);
    console.log("Services started!");

    if(environment == 'development'){
      var bootstrap = require('./bootstrap');
      bootstrap();
    }
  });

  console.log('API started: ' + app.get('port') + ' (' + environment + ')');
});

var openDatabase = function(callback){
  var db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open', function(){
    console.log('Database connection succeeded');
    console.log('Starting Services...');
    callback();
  });
};
