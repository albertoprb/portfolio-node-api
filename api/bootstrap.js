var _ = require('underscore');
var TestHelper = require('./test/helper/TestHelper');

console.log("####### STARTING TO BOOTSTRAP APP WITH DATA ######");

var bootstrap = {};

/**
 *  Create users
 */

var User = require('./model/User');
var UserFixture = require('./test/fixtures/UserFixture');

bootstrap.setUpUsers = function(callback){
  var users = [];
  _(2).times(function(n){
    var user = new User(UserFixture.basic);
    user.email = n + "@fleetster.net";
    user.save(function (err, user) {
      console.log("User created: " + user.email);
      users.push(user);
      if(_.size(users) == 2) callback(users);
    });
  });
};

/**
 *  Create users
 */

var Trip = require('./model/Trip');
var TripFixture = require('./test/fixtures/TripFixture');

var trips = [];

bootstrap.setUpTrips = function(users){
  _.each(users, function(user){
	  _(12).times(function(n){
      var trip = new Trip(TripFixture.business);
      trip.user_id = user.id;
      trip.start_location = TripFixture.randomLocation();
      trip.end_location = TripFixture.randomLocation();
      trip.start_time = TripFixture.randomStartDate();
      trip.end_time = TripFixture.randomEndDate(trip.start_time);
      trip.start_mileage = 100*(n+1);
      trip.final_mileage = 100*(n+1) + 50;
      trip.save(function (err, trip) {
        if (!err) {
          console.log("User: " + user.email + " - Trip " + n + " created: " + trip.start_location);
          trips.push(trip);
        }
        else {
          console.log("Trip error: " + err);
          console.log(err);
        }
      });
	});
  });
};

function populate() {
  clear(function(){
    console.log("......Starting to populate");
    bootstrap.setUpUsers(
		  bootstrap.setUpTrips
	  );
  });
}

function clear(callback) {
  console.log("...Cleaning previous data");
  TestHelper.cleanDatabase(callback);
}

module.exports = populate;

