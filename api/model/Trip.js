var mongoose = require('mongoose');
var moment = require('moment');
var _ = require('underscore');

var TripSchema = mongoose.Schema({
	"user_id":{type: String, required: true},
	"start_time":{type:Date, index: true, required: true},
	"end_time":{type:Date, required: true },
	"start_location":{type:String},
	"end_location":{type:String},
	"reason":{type:String},
	"partner":{type:String},
	"start_mileage":{type:String, required: true},
	"final_mileage":{type:String, required: true},
	"type":{type:String, required: true},
	"full_text":{type:String}
});

/**
 * Preparation for full text search
 */

var textSearch = require('mongoose-text-search');
TripSchema.plugin(textSearch);
TripSchema.index({ full_text: 'text' });
TripSchema.pre('save', function (next) {
  this.full_text = this.start_location + " " + this.end_location + " " + this.type + " " + this.reason + " " + this.partner;
  next();
});


var TripModel = mongoose.model('Trip', TripSchema);


/**
 * Normal validations
 */

TripModel.schema.path('end_time').validate(function (value) {
  return moment(value).isAfter(moment(this.start_time));
}, 'End time should be later than start time');

TripModel.schema.path('final_mileage').validate(function (value) {
  return this.final_mileage > this.start_mileage;
}, 'Final mileage should be bigger than initial mileage');

/**
 * Special validations
 */
TripModel.schema.path('start_time').validate(function(value, respond) {
  TripModel.find({user_id: this.user_id})
    .sort({"start_time": -1})
    .limit(1)
    .exec(function(err, trips){
      if(trips && _.size(trips) > 0){
        respond(false);
      }
      else{
        respond(true);
      }
    });
}, 'Start time before or equal to end time of last trip');

module.exports = TripModel;