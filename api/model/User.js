var mongoose = require('mongoose');

var VehicleSchema = mongoose.Schema({
	"mileage":{type: String, required: true},
	"brand":{type: String},
	"model":{type: String},
	"license_plate":{type: String}
});

var LocationsSchema = mongoose.Schema({
	"work":{type: String},
	"home":{type: String}
});

var UserSchema = mongoose.Schema({
	"email":{type: String, unique:true, required: true},
	"password":{type: String, required: true},
	"gender":{type: String},
	"first_name":{type: String},
	"last_name":{type: String},
	"vehicle":[VehicleSchema],
	"locations":[LocationsSchema],
	"access_token":{type: String}
});

function generateToken(email, password, callback){
  var bcrypt = require('bcrypt');
  bcrypt.genSalt(6, function(err, salt) {
    bcrypt.hash(email+password, salt, function(err, hash) {
      if(!err && hash) {
        callback(hash);
      }
      else {
        callback(null);
      }
    });
  });
}

UserSchema.pre('save', function (next) {
  var self = this;
  if (!this.access_token) {
    generateToken(this.email, this.password, function(generatedToken){
      self.access_token = generatedToken;
      next();
    });
  }
});

var UserModel = mongoose.model('User', UserSchema);

module.exports = UserModel;