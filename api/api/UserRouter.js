var User = require('../model/User');
var passport = require("passport");
var _ = require("underscore");
var render = require("../helper").render;
var parse = require("../helper").parse;

var router = function (app) {

    app.post('/user', function (req, res) {
      console.log("Adding user: " + req.body.email);

      var postUser = new User(req.body);
      postUser.vehicle = req.body.vehicle;
      postUser.locations = req.body.locations;
      postUser.update_number = 0;

      postUser.save(function (err, user) {
        if (!err) {
          res.send(render(user));
          return console.log("User created");
        } else {
          res.send(500);
          return console.log(err);
        }
      });
    });

	  app.post('/user/token', function (req, res) {
      console.log("Auth user: " + req.body.email);

      if(!req.body.email || !req.body.password){
        res.send(404);
        return console.log("User not found")
      }
      else {
        return User.findOne({email:req.body.email},function (err, user) {
          if(err) res.send(500);
          else if(!user) res.send(404);
          else if(user.password == req.body.password) {
            res.send(render(user));
          }
          else res.send(401);
        });
      }
	  });

  app.put('/user',
    passport.authenticate('bearer', { session: false }),
    function (req, res) {
      return User.findByIdAndUpdate(req.user.id, { $set: parse(req.body) }, function (err, user) {
        if(err) res.send(500);
        else if(!user) res.send(404);
        else {
          console.log("Updating user: " + user);
          res.send(render(user));
        }
        return console.log("User updated: " + req.user.email);
      });
    });
};

module.exports = router;