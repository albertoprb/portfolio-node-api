var _ = require('underscore');
var moment = require('moment');
var Trip = require('../model/Trip');
var User = require('../model/User');

var passport = require("passport");
var render = require("../helper").render;
var parse = require("../helper").parse;

var router = function(app) {

  // ToDo should be used for a specific trip
	app.get('/trip',
    passport.authenticate('bearer', { session: false }),
    function(req, res) {
      Trip.find({user_id: req.user.id}, {user_id: 0, _id:0, __v:0, full_text:0}, {sort: {"start_time": -1 }}, function(err, trips){
        if(err) res.send(500);
        else if(trips) { res.send({"trips": trips}); }
        else { res.send(404); }
      });
	});

	app.post('/trip',
    passport.authenticate('bearer', { session: false }),
    function(req, res) {

      if(!req.body.trips) { res.send(400); } // Bad request (Malformation)
      else if(_.size(req.body.trips) == 0) { res.send(req.body.trips); } // Empty request
      else{ saveTrips(req, res);}

	});



  /**
   * If it`s one term than we are using autocomplete with regex
   * if the user input a text we fallback to full text search
   */
  app.get('/trip/search',
    passport.authenticate('bearer', { session: false }),
    function(req, res) {

      function hasWhiteSpace(s) {
        return /\s/g.test(s);
      }

      if(!hasWhiteSpace(req.query.search)){
        if(req.query.context && req.query.context.toUpperCase() === "location".toUpperCase()) locationAutoComplete(req, res);
        else if(req.query.context && req.query.context.toUpperCase() === "reason".toUpperCase()) reasonAutoComplete(req, res);
        else if(req.query.context && req.query.context.toUpperCase() === "partner".toUpperCase()) partnerAutoComplete(req, res);
        else noContextAutoComplete(req, res);
      }
      else {
        fullTextSearch(req, res);
      }
	});

  /**
   * If it`s one term than we are using autocomplete with regex, if the user input a text we fallback to full text search
   */
  app.get('/trip/history',
    passport.authenticate('bearer', { session: false }),
    function(req, res) {
      var page = req.query.page ? req.query.page - 1 : 0;
      if(page < 0) res.send(400);
      else{
        Trip.find({user_id: req.user.id},
          {user_id: 0, _id: 0, __v: 0, full_text: 0})
          .sort({"start_time": -1})
          .skip(page * 10)
          .limit(10)
          .exec(function(err, trips){
            res.send({trips: trips});
          });
      }
    });
};

function reasonAutoComplete(req, res){
  console.log("Reason autocomplete");
  var regex = new RegExp(req.query.search, 'i');
  Trip.find({user_id: req.user.id, reason: regex},
    {user_id: 0, _id: 0, __v: 0, full_text:0})
    .sort({"start_time": -1})
    .limit(10)
    .exec(function(err, trips){
      res.send({trips: trips});
    });
}

function partnerAutoComplete(req, res){
  console.log("Partner autocomplete");
  var regex = new RegExp(req.query.search, 'i');
  Trip.find({user_id: req.user.id, partner: regex},
    {user_id: 0, _id: 0, __v: 0, full_text:0})
    .sort({"start_time": -1})
    .limit(10)
    .exec(function(err, trips){
      res.send({trips: trips});
    });
}

function locationAutoComplete(req, res){
  console.log("Location autocomplete");
  var regex = new RegExp(req.query.search, 'i');
  Trip.find({user_id: req.user.id, $or: [ {start_location: regex}, {end_location: regex}, {type: regex} ] },
    {user_id: 0, _id:0, __v:0, full_text:0})
    .sort({"start_time": -1})
    .limit(10)
    .exec(function(err, trips){
      res.send({trips: trips});
    });
}

function noContextAutoComplete(req, res){
  console.log("No context autocomplete");
  var regex = new RegExp(req.query.search, 'i');
  Trip.find({user_id: req.user.id,
      $or: [ {start_location: regex}, {end_location: regex}, {type: regex}, {reason: regex}, {partner: regex} ] },
    {user_id: 0, _id:0, __v:0, full_text:0})
    .sort({"start_time": -1})
    .limit(10)
    .exec(function(err, trips){
      res.send({trips: trips});
    });
}

function fullTextSearch(req, res) {
  console.log("Full Text Search: " + req.query.search);
  var options = {
    project: { _id: 0, __v: 0, user_id: 0, full_text:0 },
    filter: { user_id: req.user.id},
    limit: 10,
    language: 'german', // Take care (see mongodb documentation)
    lean: false
  };

  Trip.textSearch(req.query.search, options, function (err, search) {
    if(!search) res.send({trips: []});
    else res.send({trips: _.map(search.results, function (result) { return result.obj; })});
  });
}

var saveTrips = function(req, res){

  var errorTrips = [];
  var savedTrips = [];
  var numberTrips = _.size(req.body.trips);

  function respondIfFinal() {
    var trips = savedTrips.concat(errorTrips);
    if(numberTrips == _.size(trips)){
      if(_.size(errorTrips) == 0){
        console.log("All trips saved without errors");
        res.send(200, {trips: trips });
      }
      else{
        console.log("There were some errors while saving trips");
//        var errors = [];
//        _.each(trips, function(trip){
//          errors.push(trip.errors);
//        });
//        console.log(errors);
        res.send(400, {trips: trips});
      }
    }
  }

  // todo remove user_id from response
  _.each(req.body.trips, function(trip, index) {
    var newTrip = new Trip(trip);
    newTrip.user_id = req.user.id;
    newTrip.start_time = moment(trip.start_time).toDate();
    newTrip.end_time = moment(trip.end_time).toDate();
    newTrip.save( function(err, savedTrip) {
      if(err || !savedTrip) {
        console.log("Hit error while saving");

        var errMessage = [];

        // go through all the errors
        _.each(err.errors, function(error){
          errMessage.push(error.type);
        });

        var responseTrip = render(newTrip);
        responseTrip.user_id = req.user.id;
        responseTrip.errors = errMessage;
        responseTrip.valid = false;
        errorTrips.push(responseTrip);

        respondIfFinal();
      }
      else {
        console.log("No error, saved!");
        savedTrips.push(render(savedTrip));
        respondIfFinal();
      }
    });
  });
};

module.exports = router;