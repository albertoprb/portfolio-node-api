var Trip = require('../model/Trip');

var TripService = {

  searchForLocations: function(query, callback){
    Trip.textSearch(query, function (err, result) {
      callback(result);
    });
  }


};

module.exports = TripService;