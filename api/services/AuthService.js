var User = require('../model/User');
var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;
    BearerStrategy = require('passport-http-bearer').Strategy;

passport.use(new LocalStrategy(
  function(email, password, done) {
    console.log("Authenticating with email: " + email + " and password: " + password);
    User.findOne({email:email, password:password}, function (err, user) {
      if (!err && user) {
        console.log("User not found");
        done(err, user);
      }
      else{
        console.log("Error while authenticating: " + err);
        console.log("Auth with user: " + user);
        done(null, false, { message: 'Wrong email or password'});
      }
    });
  }
));

passport.use(new BearerStrategy(
  function(token, done) {
    User.findOne({ access_token: token }, function (err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      return done(null, user, { scope: 'all' });
    });
  }
));

passport.serializeUser(function(user, done) {
  console.log("Serialize user " + user);
  done(null, user.access_token);
});

passport.deserializeUser(function(token, done) {
  console.log("Deserialize user with token: " + token);
  User.findOne({access_token:token}, function (err, user) {
    console.log("User deserialized: " + user);
    done(err, user);
  });
});

module.exports = passport;
