var User = require('../model/User');

var UserService = {

	auth: function(opts, callback){
		return User.findOne(opts, function (err, user) {
			if (!err && user) {
				if(user.password == opts.password){
					console.log("Authenticated");
					return callback(user);
				}
				console.log("Wrong password");
				return callback(null);
			}
			console.log("User not found");
			console.log(err);
			return callback(null);
		});
	},

	findOne: function(opts, callback){
		return User.findOne({_id:opts._id},function (err, user) {
			if (!err && user) {
				return callback(user);
			}
			console.log("Wrong password");
			return callback(null);
		});
	}

};

module.exports = UserService;