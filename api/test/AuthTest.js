var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000');

var UserFixture = require("./fixtures/UserFixture");
var TestHelper = require("./helper/TestHelper");

describe('Testing authentication/authorization API calls', function(){

  beforeEach(TestHelper.cleanDatabase);

  describe('to /user: ', function(){

    it('should POST /user/token with correct credentials', function (done) {
      api
        .post('/user')
        .send(UserFixture.basic)
        .end(function(err, res){
          if (err) done(err);
          api
            .post('/user/token')
            .send(UserFixture.basic)
            .end(function(err, res){
              res.status.should.equal(200);
              should.exist(res.body.access_token);
              should.not.exist(res.body.password);
              should.not.exist(res.body._id);
              should.not.exist(res.body.__v);
              done();
            });
        });
    });

    it('POST /user/token without invalid or nonexistent email should fail', function (done) {
      api
        .post('/user')
        .send(UserFixture.basic)
        .end(function(err, res){
          if (err) done(err);
          api
            .post('/user/token')
            .send({email:"albertoprb@gmail.com.DE", password:"123456"})
            .expect(404);

          api
            .post('/user/token')
            .send({email:"albertoprb.com", password:"123456"})
            .expect(404, done);
        });
    });

    it('POST /user/token with incorrect password should fail', function (done) {
      api
        .post('/user')
        .send(UserFixture.basic)
        .end(function(err, res){
          if (err) done(err);
          api
            .post('/user/token')
            .send({email:"albertoprb@gmail.com", password:"12345-6789"})
            .expect(401, done);
        });
    });

    it('POST /user/token without providing password should fail', function (done) {
      api
        .post('/user')
        .send(UserFixture.basic)
        .end(function(err, res){
          if (err) done(err);
          api
            .post('/user/token')
            .send({email:"albertoprb@gmail.com"})
            .expect(404, done);
        });
    });

    it('PUT /user without token should fail', function (done) {
      api
        .post('/user')
        .send(UserFixture.basic)
        .end(function(err, res){
          if (err) done(err);
          var user = UserFixture.basic;
          user.vehicle = UserFixture.vehicle;
          api
            .put('/user')
            .send(user)
            .expect(401, done);
          });
    });

    it('PUT /user with invalid token should fail', function (done) {
      api
        .post('/user')
        .send(UserFixture.basic)
        .end(function(err, res){
          if (err) done(err);
          var user = UserFixture.complete;
          user.vehicle = UserFixture.vehicle;
          user.access_token = "1234523445DSFSDfdfsdf";
          api
            .put('/user')
            .send(user)
            .expect(401, done);
        });
    });

    it('should PUT vehicle at /user with valid token', function (done) {
      api
        .post('/user')
        .send(UserFixture.basic)
        .end(function(err, res){
          if (err) done(err);
          var user = UserFixture.complete;
          user.vehicle = UserFixture.vehicle;
          user.locations = UserFixture.locations;
          user.access_token = res.body.access_token;
          api
            .put('/user')
            .send(user)
            .end(function(err, res){
              res.status.should.equal(200);
              should.exist(res.body.vehicle);
              should.exist(res.body.locations);
              should.not.exist(res.body.password);
              done();
            });

        });
    });

    it('PUT vehicle at /user should NOT update token', function (done) {
      api
        .post('/user')
        .send(UserFixture.basic)
        .end(function(err, res){
          if (err) done(err);
          var user = UserFixture.complete;
          user.vehicle = UserFixture.vehicle;
          user.locations = UserFixture.locations;
          user.access_token = res.body.access_token;
          api
            .put('/user')
            .send(user)
            .end(function(err, res){
              res.status.should.equal(200);
              res.body.access_token.should.equal(user.access_token);
              done();
            });
        });
    });


  })
});