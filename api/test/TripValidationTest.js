var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000');

var TripFixture = require("./fixtures/TripFixture");
var UserFixture = require("./fixtures/UserFixture");

var TestHelper = require("./helper/TestHelper");

function createUser(done, callback) {
  api
    .post('/user')
    .send(UserFixture.basic)
    .end(function(err, res){
      if (err) done(err);
      callback(res);
    });
}

describe('POST /trip validation', function(){

  beforeEach(TestHelper.cleanDatabase);

  it("should not save trip with end_time < start_time", function (done) {
    createUser(done, function (res) {
      var newTrip = TripFixture.business;
      var aux = newTrip.end_time;
      newTrip.end_time = newTrip.start_time;
      newTrip.start_time = aux;

      api
        .post('/trip?access_token=' + res.body.access_token)
        .send({trips: [newTrip]})
        .end(function(err, res) {
          res.status.should.equal(400);
          res.body.trips[0].valid.should.equal(false);
          res.body.trips[0].errors[0].should.equal("End time should be later than start time");
          done();
        });
    });
  });

  it("should not save trip with final_mileage < start_mileage", function (done) {
    createUser(done, function (res) {
      var newTrip = TripFixture.business;
      var aux = newTrip.final_mileage;
      newTrip.final_mileage = newTrip.start_mileage;
      newTrip.start_mileage = aux;


      api
        .post('/trip?access_token=' + res.body.access_token)
        .send({trips: [newTrip]})
        .end(function(err, res) {
          res.status.should.equal(400);
          res.body.trips[0].valid.should.equal(false);
          res.body.trips[0].errors[0].should.equal("Final mileage should be bigger than initial mileage");
          done();
        });
    });
  });

  it("should not save trip with many errors", function (done) {
    createUser(done, function (res) {

      var invalid = TripFixture.business;
      invalid.end_time = "2085-02-04T20:44:30.652Z";
      invalid.start_time = "2085-02-04T23:44:30.652Z";
      invalid.start_mileage = "500";
      invalid.final_mileage = "400";

      api
        .post('/trip?access_token=' + res.body.access_token)
        .send({trips: [invalid]})
        .end(function(err, res) {
          res.status.should.equal(400);
          res.body.trips[0].errors.length.should.equal(2);
          res.body.trips[0].valid.should.equal(false);
          done();
        });
    });
  });

  it("should not save trip with start_time before or equal end_time of last trip (No time collision)", function (done) {
    createUser(done, function (res) {
      var user = res.body;

      var first = TripFixture.business;
      first.start_time = "2099-02-04T14:44:30.652Z";
      first.end_time = "2099-02-04T22:44:30.652Z";
      first.start_mileage = "100";
      first.final_mileage = "200";

      api
        .post('/trip?access_token=' + user.access_token)
        .send({trips: [first]})
        .end(function(err, res) {
          var invalid = TripFixture.business;
          invalid.start_time = "2099-02-04T20:44:30.652Z";
          invalid.end_time = "2099-02-04T23:44:30.652Z";
          invalid.start_mileage = "300";
          invalid.final_mileage = "400";
          api
            .post('/trip?access_token=' + user.access_token)
            .send({trips: [invalid]})
            .end(function(err, res) {
              res.status.should.equal(400);
              res.body.trips[0].valid.should.equal(false);
              res.body.trips[0].errors[0].should.equal("Start time before or equal to end time of last trip");
              done();
            });
        });
    });
  });


});