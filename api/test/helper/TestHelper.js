var MongoClient = require('mongodb').MongoClient;

var TestHelper = {

  cleanDatabase: function (done) {
    MongoClient.connect('mongodb://localhost/driverlogapi', function (err, db) {
      if (err) throw err;
      var user_collection = db.collection('users');
      var trip_collection = db.collection('trips');
      trip_collection.remove(function (err) {
        user_collection.remove(function (err) {
          done();
        });
      });
    });
  }

};

module.exports = TestHelper;