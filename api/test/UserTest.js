var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000');

var UserFixture = require("./fixtures/UserFixture");
var TestHelper = require("./helper/TestHelper");

describe('Testing API calls', function(){

  beforeEach(TestHelper.cleanDatabase);

  describe('to resource /user', function(){

    it('POST /user with email and password', function (done) {
      api
        .post('/user')
        .send(UserFixture.basic)
        .end(function(err, res){
          if (err) done(err);
          res.status.should.equal(200);
          should.exist(res.body.access_token);
          should.not.exist(res.body.password);
          done();
        });
    });

    it('POST /user without password should fail', function (done) {
      api
        .post('/user')
        .send({email:"albertoprb@gmail.com"})
        .expect(500, done)
    });

    it('POST /user without email should fail', function (done) {
      api
        .post('/user')
        .send({password:"12345"})
        .expect(500, done)
    });

    it('PUT /user with new vehicle', function (done) {
      api
        .post('/user')
        .send(UserFixture.basic)
        .end(function(err, res){
          if (err) done(err);
          var user = UserFixture.complete;
          user.vehicle = UserFixture.vehicle;
          user.access_token = res.body.access_token;
          api
            .put('/user')
            .send(user)
            .end(function(err, res){
              var vehicle = UserFixture.vehicle[0];
              res.body.vehicle[0].mileage.should.equal(vehicle.mileage);
              res.body.vehicle[0].brand.should.equal(vehicle.brand);
              res.body.vehicle[0].license_plate.should.equal(vehicle.license_plate);
              res.body.vehicle[0].model.should.equal(vehicle.model);
              done();
            });
          });
    });

    it('PUT /user with new locations', function (done) {
      api
        .post('/user')
        .send(UserFixture.basic)
        .end(function(err, res){
          if (err) done(err);
          var user = UserFixture.complete;
          user.locations = UserFixture.locations;
          user.access_token = res.body.access_token;
          api
            .put('/user')
            .send(user)
            .end(function(err, res){
              var locations = UserFixture.locations[0];
              res.body.locations[0].home.should.equal(locations.home);
              res.body.locations[0].work.should.equal(locations.work);
              done();
            });
          });
    });
  })
});