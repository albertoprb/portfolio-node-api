var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000');
var _ = require('underscore');

var TripFixture = require("./fixtures/TripFixture");
var UserFixture = require("./fixtures/UserFixture");

var TestHelper = require("./helper/TestHelper");

function createUser(done, callback) {
  api
    .post('/user')
    .send(UserFixture.basic)
    .end(function(err, res){
      if (err) done(err);
      callback(res);
    });
}
// Do not accept less than three

describe('Search/History |', function(){

  beforeEach(TestHelper.cleanDatabase);

  describe('GET /trip/search in the general context', function(){

    it('Should search for many trips with one term', function (done) {
      createUser(done, function (res) {
        var user = res.body;

        var terms = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"];
        var trips = [];

        _(10).times(function(n){

          var newTrip = TripFixture.randomTrip(n);
          newTrip.start_location = terms[n] + " " + n;
          newTrip.reason = terms[n] + " " + n;
          newTrip.partner = terms[n] + " " + n;
          trips.push(newTrip);

          if(n == 9){
            api
              .post('/trip?access_token='+user.access_token)
              .send({trips: trips})
              .end(function(err, res){
                if (err) done(err);
                else{
                  api
                    .get('/trip/search?access_token='+user.access_token+"&search="+terms[0])
                    .end(function(err, res){
                      res.status.should.equal(200);
                      res.body.trips.length.should.equal(1);
                      done();
                    });
                }
              });
          }
        });
      });
    });

    it('should not return more than 10', function (done) {
      createUser(done, function (res) {
        var user = res.body;

        var trips = [];

        _(15).times(function(n){

          var newTrip = TripFixture.randomTrip(n);
          newTrip.partner = "repeat";
          trips.push(newTrip);
          if(n == 14){
            api
              .post('/trip?access_token='+user.access_token)
              .send({trips: trips})
              .end(function(err, res){
                if (err) done(err);
                else{
                  api
                    .get('/trip/search?access_token='+user.access_token+"&search=repeat")
                    .end(function(err, res){
                      res.status.should.equal(200);
                      res.body.trips.length.should.equal(10);
                      done();
                    });
                }
              });
          }
        });
      });
    });

    it('should not be case sensitive', function (done) {
      createUser(done, function (res) {
        var user = res.body;
        var newTrip = TripFixture.randomTrip(5);
        newTrip.reason = "SEndLInger";
        api
          .post('/trip?access_token='+user.access_token)
          .send({trips: [newTrip]})
          .end(function(err, res){
            if (err) done(err);
            else{
              api
                .get('/trip/search?access_token='+user.access_token+"&search=sendlinger")
                .end(function(err, res){
                  res.status.should.equal(200);
                  res.body.trips.length.should.equal(1);
                  done();
                });
            }
          });
      });
    });

    it('should search for trip with half term', function (done) {
      createUser(done, function (res) {
        var user = res.body;
        var newTrip = TripFixture.randomTrip(5);
        newTrip.end_location = "Sendlinger";
        newTrip.reason = "Sendlinger";
        newTrip.partner = "Sendlinger";
        api
          .post('/trip?access_token='+user.access_token)
          .send({trips: [newTrip]})
          .end(function(err, res){
            if (err) done(err);
            else{
              api
                .get('/trip/search?access_token='+user.access_token+"&search="+"send")
                .end(function(err, res){
                  res.status.should.equal(200);
                  res.body.trips.length.should.equal(1);
                  done();
                });
            }
          });

      });
    });

    it('trips for one user can`t be shown in another user search', function (done) {
      function setUp(callback) {
        createUser(done, function (res) {
          var user = res.body;
          var firstTrip = TripFixture.randomTrip(1);
          firstTrip.start_location = "Sendlinger";
          firstTrip.end_location = "Sendlinger";
          firstTrip.reason = "Sendlinger";
          firstTrip.partner = "Sendlinger";
          api
            .post('/trip?access_token=' + user.access_token)
            .send({trips: [firstTrip]})
            .end(function (err, res) {
              callback(err, res);
            });
        });
      }
      setUp(function(err, res){
        api
          .post('/user')
          .send({email: "new@gmail.com", password:"123456"})
          .end(function(err, res){
            var newUser = res.body;
            var secondTrip = TripFixture.randomTrip(2);
            secondTrip.start_location = "Sendlinger";
            secondTrip.end_location = "Sendlinger";
            secondTrip.reason = "Sendlinger";
            secondTrip.partner = "Sendlinger";
            api
              .post('/trip?access_token='+newUser.access_token)
              .send({trips: [secondTrip]})
              .end(function(err, res){
                api
                  .get('/trip/search?access_token='+newUser.access_token+"&search="+"Sendlinger")
                  .end(function(err, res){
                    res.status.should.equal(200);
                    res.body.trips.length.should.equal(1);
                    done();
                  });
              });
          });
      });
    });
  });

  describe('GET /trip/search in the location context |', function(){

    it('should search for trip start and end locations with one term', function (done) {
      createUser(done, function (res) {
        var user = res.body;
        var newTrip = TripFixture.randomTrip(5);
        newTrip.start_location = "SEndLInger Start";
        newTrip.end_location = "SEndLInger Stop";
        api
          .post('/trip?access_token='+user.access_token)
          .send({trips: [newTrip]})
          .end(function(err, res){
            if (err) done(err);
            else{
              api
                .get('/trip/search?access_token='+user.access_token+"&context=location&search=sendlinger")
                .end(function(err, res){
                  res.status.should.equal(200);
                  res.body.trips.length.should.equal(1);
                  done();
                });
            }
          });
      });
    });

    it('should search for trip locations with the type of trip', function (done) {
      createUser(done, function (res) {
        var user = res.body;
        var newTrip = TripFixture.randomTrip(5);
        api
          .post('/trip?access_token='+user.access_token)
          .send({trips: [newTrip]})
          .end(function(err, res){
            if (err) done(err);
            else{
              api
                .get('/trip/search?access_token='+user.access_token+"&context=location&search="+"Business")
                .end(function(err, res){
                  res.status.should.equal(200);
                  res.body.trips.length.should.equal(1);
                  done();
                });
            }
          });
      });
    });

  });

  describe('GET /trip/search in the reason context', function(){

    it('should search for trip reasons', function (done) {
      createUser(done, function (res) {
        var user = res.body;
        var newTrip = TripFixture.randomTrip(5);
        newTrip.reason = "ReaSon";
        api
          .post('/trip?access_token='+user.access_token)
          .send({trips: [newTrip]})
          .end(function(err, res){
            if (err) done(err);
            else{
              api
                .get('/trip/search?access_token='+user.access_token+"&context=reason&search=reas")
                .end(function(err, res){
                  res.status.should.equal(200);
                  res.body.trips.length.should.equal(1);
                  done();
                });
            }
          });

      });
    });

  });

  describe('GET /trip/search in the partner context', function(){

    it('should search for trip partners', function (done) {
      createUser(done, function (res) {
        var user = res.body;
        var newTrip = TripFixture.randomTrip(5);
        newTrip.partner = "partNER";
        api
          .post('/trip?access_token='+user.access_token)
          .send({trips: [newTrip]})
          .end(function(err, res){
            if (err) done(err);
            else{
              api
                .get('/trip/search?access_token='+user.access_token+"&context=partner&search=par")
                .end(function(err, res){
                  res.status.should.equal(200);
                  res.body.trips.length.should.equal(1);
                  done();
                });
            }
          });

      });
    });

  });

  describe('GET /trip/search with full text search', function(){

    it('should use full text search when there are many terms', function (done) {
      createUser(done, function (res) {
        var user = res.body;

        var terms = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"];
        var trips = [];

        _(10).times(function(n){

          var newTrip = TripFixture.randomTrip(n);
          newTrip.start_location = terms[n] + " " + n;
          trips.push(newTrip);
          if(n == 9){
            api
              .post('/trip?access_token='+user.access_token)
              .send({trips: trips})
              .end(function(err, res){
                if (err) done(err);
                else{
                  api
                    .get('/trip/search?access_token='+user.access_token+"&search="+terms[0]+" "+terms[1]+" "+terms[2])
                    .end(function(err, res){
                      res.status.should.equal(200);
                      res.body.trips.length.should.equal(3);
                      done();
                    });
                }
              });
          }
        });
      });
    });

    it('should return one item on this case and two with common term', function (done) {
      createUser(done, function (res) {
        var user = res.body;

        var first = TripFixture.randomTrip(5);
        first.start_location = "one";
        first.end_location = "two";
        first.type = "three";
        first.partner = "same";
        first.reason = "common";

        var second = TripFixture.randomTrip(5);
        second.start_location = "common";
        second.end_location = "same";
        second.type = "six";
        second.partner = "seven";
        second.reason = "eight";

        api
          .post('/trip?access_token='+user.access_token)
          .send({trips: [first, second]})
          .end(function(err, res){
            if (err) done(err);
            else{
              api
                .get('/trip/search?access_token='+user.access_token+"&search=one two three")
                .end(function(err, res){
                  res.status.should.equal(200);
                  res.body.trips.length.should.equal(1);
                });

              api
                .get('/trip/search?access_token='+user.access_token+"&search=six seven eight")
                .end(function(err, res){
                  res.status.should.equal(200);
                  res.body.trips.length.should.equal(1);
                });

              api
                .get('/trip/search?access_token='+user.access_token+"&search=one eight")
                .end(function(err, res){
                  res.status.should.equal(200);
                  res.body.trips.length.should.equal(2);
                });

              api
                .get('/trip/search?access_token='+user.access_token+"&search=common same")
                .end(function(err, res){
                  res.status.should.equal(200);
                  res.body.trips.length.should.equal(2);
                  done();
                });
            }
          });

        });
      });
    });

    it('should not behave like autocomplete', function (done) {
      createUser(done, function (res) {
        var user = res.body;
        var newTrip = TripFixture.randomTrip(5);
        newTrip.reason = "reason";
        newTrip.partner = "partner";
        newTrip.start_location = "SEndLInger sTART";
        newTrip.end_location = "SEndLInger STOP";
        api
          .post('/trip?access_token='+user.access_token)
          .send({trips: [newTrip]})
          .end(function(err, res){
            if (err) done(err);
            else{
              api
                .get('/trip/search?access_token='+user.access_token+"&search=send rea part sto sta")
                .end(function(err, res){
                  res.status.should.equal(200);
                  res.body.trips.length.should.equal(0);
                  done();
                });
            }
          });
      });
    });

  describe('GET /trip/history', function(){

    it('should list the past 10 trips', function (done) {
      createUser(done, function (res) {
        var user = res.body;

        var terms = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve"];
        var mileage = ["100", "200", "300", "400", "500", "600", "700", "800", "900", "1000", "1100", "1200"];
        var trips = [];

        _(12).times(function(n){

          var newTrip = TripFixture.randomTrip(n);
          newTrip.start_location = terms[n] + " " + n;
          newTrip.reason = terms[n] + " " + n;
          newTrip.partner = terms[n] + " " + n;
          newTrip.start_mileage = mileage[n];
          newTrip.final_mileage = mileage[n] + 50;
          trips.push(newTrip);

          if(n == 11){
            api
              .post('/trip?access_token='+user.access_token)
              .send({trips: trips})
              .end(function(err, res){
                if (err) done(err);
                else{
                  api
                    .get('/trip/history?access_token='+user.access_token)
                    .end(function(err, res){
                      res.status.should.equal(200);
                      res.body.trips.length.should.equal(10);
                    });

                  api
                    .get('/trip/history?access_token='+user.access_token+"&page=0")
                    .end(function(err, res){
                      res.status.should.equal(400);
                    });

                  api
                    .get('/trip/history?access_token='+user.access_token+"&page=2")
                    .end(function(err, res){
                      res.status.should.equal(200);
                      res.body.trips.length.should.equal(2);
                      done();
                    });
                }
              });
          }
        });
      });
    });

  });

});