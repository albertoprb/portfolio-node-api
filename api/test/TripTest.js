var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000');

var TripFixture = require("./fixtures/TripFixture");
var UserFixture = require("./fixtures/UserFixture");

var TestHelper = require("./helper/TestHelper");

function createUser(done, callback) {
  api
    .post('/user')
    .send(UserFixture.basic)
    .end(function(err, res){
      if (err) done(err);
      callback(res);
    });
}

describe('Testing resource /trip', function(){

  beforeEach(TestHelper.cleanDatabase);

  it('POST should not work without token', function (done) {
    createUser(done, function (res) {
      api
        .post('/trip')
        .send({trips:[TripFixture.basic]})
        .expect(401, done)
    });
  });

  it('POST with invalid token should not work', function (done) {
    createUser(done, function (res) {
      api
        .post('/trip?access_token='+'520ddb2b1682e93107000025')
        .send({trips:[TripFixture.basic]})
        .expect(401);

      api
        .post('/trip')
        .send({access_token:'520ddb2b1682e93107000025', trips:[TripFixture.basic]})
        .expect(401);

      api
        .post('/trip')
        .set('Authorization', 'Bearer 520ddb2b1682e93107000025')
        .send({trips:[TripFixture.basic]})
        .expect(401, done);
    });
  });

  it('POST without body should not work', function (done) {
    createUser(done, function (res) {
      api
        .post('/trip?access_token='+res.body.access_token)
        .send({})
        .expect(400, done)
    });
  });

  it('POST with body malformation should not work', function (done) {
    createUser(done, function (res) {
      api
        .post('/trip?access_token='+res.body.access_token)
        .send(TripFixture.business) // Not array
        .expect(400, done)
    });
  });

  it('POST with no trips should return status code 200', function (done) {
    createUser(done, function (res) {
      api
        .post('/trip?access_token='+res.body.access_token)
        .send({trips:[]})
        .expect(200, done)
    });
  });

  it('POST with business trip', function (done) {
    createUser(done, function (res) {
      var user = res.body;
      api
        .post('/trip?access_token='+user.access_token)
        .send({trips:[TripFixture.business]})
        .end(function(err, res){
          res.status.should.equal(200);
          api
            .get('/trip?access_token='+user.access_token)
            .end(function(err, res){
              res.body.trips.length.should.equal(1);
              done();
            });
        });
    });
  });

  it('POST with private trip', function (done) {
    createUser(done, function (res) {
      var user = res.body;
      api
        .post('/trip?access_token='+user.access_token)
        .send({trips:[TripFixture.private]})
        .end(function(err, res){
          res.status.should.equal(200);
          api
            .get('/trip?access_token='+user.access_token)
            .end(function(err, res){
              res.body.trips.length.should.equal(1);
              done();
            });
        });
    });
  });

  it('POST with home-work trip', function (done) {
    createUser(done, function (res) {
      var user = res.body;
      api
        .post('/trip?access_token='+user.access_token)
        .send({trips:[TripFixture.home_work]})
        .end(function(err, res){
          res.status.should.equal(200);
          api
            .get('/trip?access_token='+user.access_token)
            .end(function(err, res){
              res.body.trips.length.should.equal(1);
              done();
            });
        });
    });
  });

  it('POST with many trips', function (done) {
    createUser(done, function (res) {
      var user = res.body;
      api
        .post('/trip?access_token='+user.access_token)
        .send({trips:[TripFixture.business, TripFixture.private, TripFixture.home_work]})
        .end(function(err, res){
          res.status.should.equal(200);
          api
            .get('/trip?access_token='+user.access_token)
            .end(function(err, res){
              res.body.trips.length.should.equal(3);
              done();
            });
        });
    });
  });

});