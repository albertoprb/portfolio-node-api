var mongoose = require('mongoose');
var moment = require('moment');

var Trip = {

  locations: [
    "Zielstattstraße 61, Büropark Obersendling, 81379 Munich, Germany",
    "Plattlinger Straße 9, 81479 München, Germany‎",
    "Ackermannstraße 19, Munich, Germany",
    "Altes Hackerhaus, Sendlinger Straße 14, 80331 München, Deutschland",
    "Implerstraße 12A, Munich, Germany",
    "Zillertalstraße 40 81373 München, Germany",
    "Fernpaßstraße 40 81373 München, Germany",
    "Garmischer Straße 284 81377 München, Germany",
    "Höglwörther Straße 266 81379 München, Germany",
    "Ambacher Straße 14 81476 München, Germany",
    "Sendlinger-Tor-Platz 10 80336 München, Germany"
  ],

  randomLocation: function(){
    return this.locations[Math.floor(Math.random()*11)];
  },

  randomStartDate: function(){
	  var startDate = moment();
	  startDate.subtract('hours', Math.floor(Math.random()*12));
    startDate.year(2000 + Math.floor(Math.random()*100));
    return startDate.toDate();
  },

  randomEndDate: function(startDate){
    var endDate = moment(startDate);
	  endDate.add('hours', 1 + Math.floor(Math.random()*12));
    return endDate.toDate();
  },

  randomTrip: function(n){
    var start_time = this.randomStartDate();
    return {
      start_time: start_time,
      end_time: this.randomEndDate(start_time),
      start_location: this.randomLocation(),
      end_location: this.randomLocation(),
      start_mileage: 100*(n+1),
      final_mileage: 100*(n+2),
      reason: "reason",
      partner: "partner",
      type: "Business Trip"
    }
  },

  business: {
    start_time: "2013-02-04T14:44:30.652Z",
    end_time: "2013-02-04T22:44:30.652Z",
    start_location: "Zielstattstraße 61, Büropark Obersendling, 81379 Munich, Germany",
    end_location: "Plattlinger Straße 9, 81479 München, Germany‎",
    start_mileage: "100",
    final_mileage: "200",
    reason: "reason",
    partner: "partner",
    type: "Business Trip"
  },

  private: {
    start_time: "2013-02-04T05:44:30.652Z",
    end_time: "2013-02-04T09:44:30.652Z",
    start_mileage: "0",
    final_mileage: "50",
    type: "Private Trip"
  },

  home_work: {
    start_time: "2013-02-04T12:44:30.652Z",
    end_time: "2013-02-04T14:44:30.652Z",
    start_location: "Ackermannstraße 19, Munich, Germany",
    end_location: "Zielstattstraße 19A, Büropark Obersendling, 81379 Munich, Germany",
    type: "Home To Work",
    start_mileage: "500",
    final_mileage: "580"
  },

  work_home: {
    start_time: "2013-02-04T16:44:30.652Z",
    end_time: "2013-02-04T18:44:30.652Z",
    start_location: "Zielstattstraße 19A, Büropark Obersendling, 81379 Munich, Germany",
    end_location: "Ackermannstraße 19, Munich, Germany",
    type: "Work To Home",
    start_mileage: "500",
    final_mileage: "700"
  },

  many: {
    trips: [this.business, this.private, this.home_work, this.work_home]
  }
};

module.exports = Trip;