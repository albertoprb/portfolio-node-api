var _ = require('underscore');

var user = {

  randomEmail: function(){
    return "alberto" + _.random(0, 1000000) + "@gmail.com";
  },

  basic: {
    "email": "albertoprb@gmail.com",
    "password": "123456"
  },

  profile: {
    "first_name": "alberto",
    "last_name": "barcelos",
    "gender": "ms."
  },

  vehicle: [{
    "mileage": "10001",
    "brand": "BMW",
    "model": "some",
    "license_plate": "anything"
  }],

  locations: [{
    "work": "newaddress",
    "home": "newaddress"
  }],

  complete: {
    "email": "albertoprb@gmail.com",
    "first_name": "alberto",
    "gender": "ms.",
    "last_name": "barcelos",
    "password": "pass",
    "locations": this.locations,
    "vehicle": this.vehicle
  }

};

module.exports = user;