var _ = require('underscore');

var helpers = {

  render: function(mongooseModel){
    return _.omit(mongooseModel.toObject(), ['_id', '__v', 'password']);
  },

  parse: function(object){
    return _.omit(object, ['_id', '__v', 'password']);
  }


};

module.exports = helpers;